import React from 'react';
import SearchWidget from './SearchWidget';

describe('<SearchWidget />', () => {
  const wrapper = shallow(<SearchWidget />);
  it('(AC1) Should be a text box labelled "Pick-up Location" ', () => {
    expect(wrapper.find('label').text()).toEqual('Pick-up Location');
    expect(wrapper.find('input')).toHaveLength(1);
  });
  it('(AC2) Should have the correct placehollder text" ', () => {
    expect(wrapper.find('input').props().placeholder).toEqual(
      'city, airport, station, region and district...',
    );
  });
});
