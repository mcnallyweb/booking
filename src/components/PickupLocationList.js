import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './PickupLocations.scss';

const getLocationSubtext = (location) => {
  const { city, region, country } = location;
  return [city, region, country].filter(item => item !== undefined).join(', ');
};

class PickupLocationList extends Component {
  state = {};

  render() {
    const { locations, numFound } = this.props;
    if (numFound === 0 && locations.length === 1) {
      return (
        <ul className="list-unstyled pickup-locations">
          <li>
            <p className="mb-0">No results found</p>
          </li>
        </ul>
      );
    }

    return (
      <ul className="list-unstyled pickup-locations" role="listbox">
        {locations.map((location) => {
          const locationText = getLocationSubtext(location);
          const { name, bookingId } = location;
          return (
            <li role="option" aria-selected="false" key={bookingId}>
              <button type="button">
                <p className="mb-0">{name}</p>
                <p className="small mb-0">{`${locationText} `}</p>
              </button>
            </li>
          );
        })}
      </ul>
    );
  }
}

PickupLocationList.propTypes = {
  locations: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      city: PropTypes.string,
      region: PropTypes.string,
      country: PropTypes.string,
      bookingId: PropTypes.string.isRequired,
    }),
  ),
  numFound: PropTypes.number,
};

PickupLocationList.defaultProps = {
  locations: [],
  numFound: 0,
};

export default PickupLocationList;
