import React from 'react';
import SearchWidget from './components/SearchWidget';

const App = () => (
  <div>
    <SearchWidget />
  </div>
);

export default App;
