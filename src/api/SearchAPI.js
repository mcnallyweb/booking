import axios from 'axios';

const PICKUP_LOCATION_SEARCH = 'https://cors.io/?https://www.rentalcars.com/FTSAutocomplete.do?solrIndex=fts_en&';

export default {
  getPickupLocations: async (location, limit = 5) => {
    const url = `${PICKUP_LOCATION_SEARCH}solrRows=${limit}&solrTerm=${location}`;
    try {
      const result = await axios.get(url);
      return { locations: result.data.results.docs, numFound: result.data.results.numFound };
    } catch (error) {
      return [];
    }
  },
};
