import React, { Component } from 'react';
import { debounce } from 'lodash';
import { polyfill } from 'es6-promise';
import PickupLocationList from './PickupLocationList';
import searchAPI from '../api/SearchAPI';
import './SearchWidget.scss';

polyfill();

export default class SearchWidget extends Component {
  state = {
    locations: [],
    numFound: 0,
  };

  pickupSearchInput = React.createRef();

  /**
   * Wrapping in debounce to prevent unnecasary calls to the api
   * when typing quickly
   */
  handleOnChange = debounce(async () => {
    const searchTerm = this.pickupSearchInput.current.value;
    if (searchTerm.length <= 1) {
      this.setState({
        locations: [],
      });
      return;
    }
    const result = await searchAPI.getPickupLocations(searchTerm, 6);
    // Check that the input hasnt reduced while calling the API
    if (this.pickupSearchInput.current.value.length > 1) {
      const { numFound, locations } = result;
      this.setState({
        locations,
        numFound,
      });
    }
  }, 100);

  render() {
    const { locations, numFound } = this.state;
    return (
      <div className="search-widget">
        <h2>Lets find your ideal car</h2>
        <div className="form-group">
          <label htmlFor="pickupLocation">Pick-up Location</label>
          <input
            type="text"
            autoComplete="off"
            aria-required="true"
            aria-haspopup="true"
            aria-autocomplete="list"
            className="form-control"
            id="pickupLocation"
            placeholder="city, airport, station, region and district..."
            ref={this.pickupSearchInput}
            onChange={this.handleOnChange}
          />
          {locations.length > 0 && <PickupLocationList numFound={numFound} locations={locations} />}
        </div>
      </div>
    );
  }
}
